# Changelog

All remarkable changes on this project will be documented in this file

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

<!--
## [UNRELEASED]
### Added
### Changed
### Deprecated
### Removed
### Fixed
### Security
-->



## v2.0.1 - 2020-01-26

- Minor fixes


## v2.0.0 - 2020-01-20

- New production version of Movies test exercise using custom written components instead of qTable by Quasar


## v1.0.1 - 2020-01-12

- Minor fixes


## v1.0.0 - 2020-01-12

- First production version of Movies test exercise