import { createLocalVue, shallowMount } from "@vue/test-utils";
import * as All from "quasar";
import CmtPager from "@/components/CmtPager.vue";

const { Quasar } = All;

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key];

  if (val && val.component && val.component.name !== null) {
    object[key] = val;
  }
  return object;
}, {});

const localVue = createLocalVue();
localVue.use(Quasar, { components });

describe("CmtPager", () => {
  it("is a Vue instance", () => {
    const wrapper = shallowMount(CmtPager, {
      computed: {
        disablePrevButton: () => false,
        disableNextButton: () => false
      },
      localVue,
      attachToDocument: true
    });
    expect(wrapper.isVueInstance()).toBeTruthy();
  });
});

describe("on page changed", () => {
  let wrapper;
  const stub = jest.fn();

  beforeAll(() => {
    wrapper = shallowMount(CmtPager, {
      computed: {
        disablePrevButton: () => false,
        disableNextButton: () => false
      },
      localVue,
      attachToDocument: true
    });

    wrapper.setMethods({ calculatePaginationValues: stub });
    wrapper.setData({ page: 5 });
  });

  it("should call calculatePaginationValues()", () => {
    expect(stub).toBeCalled();
  });

  it("should emit page-selected-changed event", () => {
    expect(wrapper.emitted("page-selected-changed")).toBeTruthy();
  });
});

describe("on called handlePage with next value", () => {
  let wrapper;

  beforeAll(() => {
    wrapper = shallowMount(CmtPager, {
      computed: {
        disablePrevButton: () => true,
        disableNextButton: () => false
      },
      localVue,
      attachToDocument: true
    });

    wrapper.setData({ page: 1 });
    wrapper.setProps({ lastPage: 5 });
    wrapper.vm.handlePage("next");
  });

  it("page value should be increased by one", () => {
    expect(wrapper.vm.page).toBe(2);
  });
});

describe("on records per page changed", () => {
  let wrapper;
  const stub = jest.fn();

  beforeAll(() => {
    wrapper = shallowMount(CmtPager, {
      computed: {
        disablePrevButton: () => false,
        disableNextButton: () => false
      },
      localVue,
      attachToDocument: true
    });

    wrapper.setMethods({ calculatePaginationValues: stub });
    wrapper.setData({ recordsPerPage: 25 });
  });

  it("should call calculatePaginationValues()", () => {
    expect(stub).toBeCalled();
  });

  it("should emit records-per-page-changed event", () => {
    expect(wrapper.emitted("records-per-page-changed")).toBeTruthy();
  });
});
