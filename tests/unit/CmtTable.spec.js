import { createLocalVue, shallowMount } from "@vue/test-utils";
import * as All from "quasar";
import CmtTable from "@/components/CmtTable.vue";

const { Quasar } = All;

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key];

  if (val && val.component && val.component.name !== null) {
    object[key] = val;
  }
  return object;
}, {});

const localVue = createLocalVue();
localVue.use(Quasar, { components });

describe("CmtTable", () => {
  it("is a Vue instance", () => {
    const wrapper = shallowMount(CmtTable, {
      localVue,
      attachToDocument: true
    });
    expect(wrapper.isVueInstance()).toBeTruthy();
  });
});
