import { createLocalVue, shallowMount } from "@vue/test-utils";
import * as All from "quasar";
import CmtSearch from "@/components/CmtSearch.vue";

const { Quasar } = All;

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key];

  if (val && val.component && val.component.name !== null) {
    object[key] = val;
  }
  return object;
}, {});

const localVue = createLocalVue();
localVue.use(Quasar, { components });

describe("CmtSearch", () => {
  it("is a Vue instance", () => {
    const wrapper = shallowMount(CmtSearch, {
      localVue,
      attachToDocument: true
    });
    expect(wrapper.isVueInstance()).toBeTruthy();
  });
});
