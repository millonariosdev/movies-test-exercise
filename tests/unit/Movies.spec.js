import { createLocalVue, shallowMount } from "@vue/test-utils";
import * as All from "quasar";
import Movies from "@/components/Movies.vue";

const { Quasar } = All;

const components = Object.keys(All).reduce((object, key) => {
  const val = All[key];

  if (val && val.component && val.component.name !== null) {
    object[key] = val;
  }
  return object;
}, {});

const localVue = createLocalVue();
localVue.use(Quasar, { components });

describe("Movies", () => {
  it("is a Vue instance", () => {
    const wrapper = shallowMount(Movies, {
      computed: {
        data: () => () => []
      },
      localVue,
      attachToDocument: true
    });
    expect(wrapper.isVueInstance()).toBeTruthy();
  });
});

describe("Method removeDuplicatedMovies()", () => {
  let filteredMovies = [];
  const longMockedMoviesArray = [
    {
      title: "ABC",
      director: "Alan Smithee"
    },
    {
      title: "XYZ",
      director: "Jane Doe"
    },
    {
      title: "ABC",
      director: "Alan Smithee"
    }
  ];
  it("remove duplicate objects in movies array", () => {
    const wrapper = shallowMount(Movies, {
      computed: {
        data: () => () => []
      },
      localVue,
      attachToDocument: true
    });
    filteredMovies = wrapper.vm.removeDuplicatedMovies(longMockedMoviesArray);
    expect(filteredMovies).toEqual([
      {
        title: "ABC",
        director: "Alan Smithee"
      },
      {
        title: "XYZ",
        director: "Jane Doe"
      }
    ]);
  });
});
