# movies-test-exercise

## Technology

- **VueJS** as javascript framework
- **Quasar** provides a set of elements to markup and style your app, so i chose it for increasing develop speed.

### Goals

After my first version with a Quasar table and your feedback, i took the long road, which meant developing all the custom components myself and communicate them to achieve the exercise goals, always having in mind your focus on performance and readability.

From my previous version i kept the logic for composing a merged array of both movies lists and the duplication removal method. But i took other directions:

1. Running the removeDuplicates method on the full array of movies freezed the browser so i decided to create two arrays, one storing all the movies and the other collecting ONLY the parsed data by the criteria selected by the user (sort, pagination and filtering). This helped to have a performant version, responding fast to any change entered.

2. Decided to create 3 main components: cmt-table, cmt-pager and cmt-search, communicating all of them between them and with the main view (the de facto controller) using props and events. Though the architecture is light and have almost no layers, having 3 levels (Movies.vue => cmt-table-vue => cmt-search.vue|cmt-pager.vue) forced me to add maybe an extra number of props, situation avoidable by using an event bus or Vuex. Anyway, this situation happened very few times, so i decided to keep it simple.

I used a quasar prop (:grid="$q.screen.xs") to facilitate table data readibility in mobile devices.

Also added a Changelog to include a list of release notes.

### Improvement points

- Prepare a responsive version using cards instead of a table to increase readability.
- Get total test coverage for all new components. I added a few more tests but there was no time to get full coverage. Nice to have.
- Improve th ordering clicking display by adding some arrow icon.

### Evolution points

- i18n
- Environment structure (envalid, config files) to have different environment variables depending on the environment we are running

### Demo

Delivered a production running version to Netlify so you can test online the exercise.

[https://movies-test-exercise.netlify.com//](https://movies-test-exercise.netlify.com/)


## Project setup

```

npm install

```

### Compiles and hot-reloads for development

```

npm run serve

```

### Compiles and minifies for production

```

npm run build

```

### Run your tests

```

npm run test:unit

```

### Lints and fixes files

```

npm run lint

```

### Customize configuration

See [Configuration Reference](https://cli.vuejs.org/config/).